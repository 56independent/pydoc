def find(directory, extension):
    '''
    Looks for files of extension extension, within directory.

    ```py
    find("my/beloved/files", ".py")
    ```

    Returns a list and contents:

    ```py
    [["a.py", "b.py", "c.py"], "a.py b.py c.py"]
    ```
    '''

    import re
    import os

    contents = ""
    for i in range(len(os.listdir(directory))):
        contents += os.listdir(directory)[i] + " "

    print(contents)

    match = re.match(".*" + extension + " ", contents)

    print(match)

    return([match, contents])


def readFile(filename):
    '''
    Takes a file, reads it for docstrings in functions, and then returns a list containing the define statement and docstring in assorted tuples:

    ```py
    [("Introduction", "Lorum ispum dolor sit amet"), ("Function x", "Does y to x variable")]
    ```
    '''

    import re

    with open(filename + ".py", "r") as f:
        matching = re.findall("\s*def (.*):[\s]*['\"]{3}([\s\S]*?)['\"]{3}", f.read())

    found = matching

    return found

def createMarkdown(found, markdown = ""):
    '''
    Uses a list containing tuples containing header and body text and creates markdown.
    
    ```py
    [("Introduction", "Lorum ispum dolor sit amet"), ("Function x", "Does y to x variable")]
    ```

    An optional `markdown` argument can be given, adding the text to markdown.
    '''
    import re

    if markdown != "":
        markdown += "\n\n"

    for i in range(len(found)):
        body = found[i][1]
        body = re.sub("\n[\t ]*", "\n", body) # Remove beggining indentation
        #body = "\n" + body

        markdown += "# " + found[i][0] + "\n" + body + "\n\n"

    return markdown

def compileMarkdown(markdown):
    '''
    Wrapper function for marko.convert. 
    '''

    import marko

    html = marko.convert(markdown)

    return html
    # return("<h1>404 Error</h1>")

def index(directory, write = False, parse = False):
    '''
    Given a directory, generates an index of pydoc files available in given diectory.

    If `write` is true, then writes the PyDoc files for the files in the directory.

    If `parse` is true, parses the markdown instead of returning it.
    '''
    
    import re

    matches = find(directory)

    markdown = "# Index\n"

    for i in matches:
        file = match[i]

        file = re.sub("\.py", "\.md", file)

        markdown += "(" + file + ")[./" + file + "]\n"

    print(markdown)

    if write:
        import l3
        l3.recurse(directory)

    if parse:
        compileMarkdown(markdown)
    else:
        return markdown
