def createMarkdown(file, parse = False):
    '''
    Given a file, creates markdown for the file, and compiles markdown if true is given.
    '''
    
    import l1

    stuff = l1.readFile(file)
    markdown = l1.createMarkdown(stuff)

    with open(file + ".md", "w") as mdfile:
        mdfile.write(markdown)

    if parse:
        html = l1.compileMarkdown(markdown)
        
        with open(file + ".html", "w") as htmlfile:
            htmlfile.write(html)
        
    else:
        return markdown
        
import sys

createMarkdown(sys.argv[1], True)