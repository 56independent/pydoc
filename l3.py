import l1
import l2

def recurse(directory):
    ''''
    Looks through a directory, finds all `.py` files, and generates markdown for each one.
    '''

    match = l1.find(directory,  ".py")[0]

    print(match)

    for i in match:
        file = match[i]
        l2.createMarkdown(file, True)

import sys

recurse(sys.argv[1])