# Introduction
PyDoc introduces scripts making documentation of large projects easier.

# Installation
To install PyDoc, clone this repository, using `git clone https://gitlab.com/56independent/pydoc` into a folder of your choice. From that folder, you can execute the commands below.

# Usage
Pydoc is used by executing python scripts with arbituary arguments.

To document a single file, run `python3 l2.py "path/to/file"` with no extension.

To document all files in a directory, run `python3 l3.py "path/to/directory`. The

To hand-document a file, run `ed path/to/file.md`, with the file named differently from the source (as keeping it the same will force pydoc to overwrite the file).

# Code
Code which needs documentation must have functions and within those functions docstrings. The dostrings are what are documented.

## Examples
For example, code which will be documented is:

```py
def readFile(filename):
    '''
    Takes a file, reads it for docstrings in functions, and then returns a list containing the define statement and docstring
    '''
```

Code which will not be documented is:

```py
'''
This file adds level 1 functions.
'''
```

or

```py
def readFile(filename):
    # Takes a file, reads it for docstrings in functions, and then returns a list containing the define statement and docstring
```

More explicitly, the regex to find the docstrings and function declerations is: `

```re
\s*def (.*):[\s]*['\"]{3}([\s\S]*?)['\"]{3}
```

## Docstring standards
Docstrings should be written in markdown, as the program takes the docstring and shoves it into the body text, with no processing (except whitespace tidying).

Please look up a markdown tutorial if looking for guidance on creating markdown

# For PyDoc developers
PyDoc, except this file, has documented itself and its functions. You may use these to find out how to use functions.

The documentation cycle usually goes like this:

```
l2 file executed with file as argument

l2 calls l1 functions which:

    take the file, and take out the docstrings and function headers

    take the docstrings and function headers and convert to markdown

l2 saves the markdown and may call an l1 function which

    takes the markdown and turns it into HTML
```